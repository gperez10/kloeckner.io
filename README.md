# Javier Pérez assignment :)

The goal of this document is to explain the development of the task that was assigned to me

1. Structure / architecture
  - Observable Pattern
  - Redux Pattern
2. Libs
- NGRX
- Spectator for unit test
- Angular Material
3. Provide an overview which points you would improve if you had more time to work on the project
- I had some problems with spectator, the goal was finish all points
- I will do the e2e
- I will do more robust the State
- I will apply the TDD
- Implement Smart and Dump components

4.Post Script
- It was a nice and challenging test, thanks for the opportunity

![Car Image](src/assets/image/ui.png)
