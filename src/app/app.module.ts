import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatDialogModule } from '@angular/material/dialog';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatDividerModule } from '@angular/material/divider';
import { WeatherHomeComponent } from './weather/weather-home/weather-home.component';
import { HttpConfigInterceptor } from './services/http-config.interceptor';
import { MatToolbarModule } from '@angular/material/toolbar'
import { meteorologyReducer } from './store/meteorology.reducer';
import { StoreModule } from '@ngrx/store';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { EffectsModule } from '@ngrx/effects';
import { MeteorologyEffect } from './store/meteorology.effects';
import { WeatherService } from './services/weather/weather.service';
import { AppRoutingModule } from './app-routing.module';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { ErrorModalHandlerComponent } from './share/modal/error-modal-handler/error-modal-handler.component';
import { ErrorDialogService } from './services/error-handler/error-handler.service';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatSelectModule } from '@angular/material/select';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { ReactiveFormsModule } from '@angular/forms';
import { MatListModule } from '@angular/material/list';
import { MatGridListModule } from '@angular/material/grid-list';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    WeatherHomeComponent,
    ErrorModalHandlerComponent,
  ],
  imports: [
    BrowserModule,
    FlexLayoutModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    StoreModule.forRoot({ meteorology: meteorologyReducer }),
    StoreDevtoolsModule.instrument(),
    HttpClientModule,
    EffectsModule.forRoot([MeteorologyEffect]),
    MatToolbarModule,
    MatDialogModule,
    MatCardModule,
    MatButtonModule,
    MatAutocompleteModule,
    MatSelectModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatInputModule,
    MatListModule,
    MatGridListModule,
    MatDividerModule
  ],

  providers: [WeatherService, { provide: HTTP_INTERCEPTORS, useClass: HttpConfigInterceptor, multi: true }, ErrorDialogService],
  bootstrap: [AppComponent]
})
export class AppModule { }
