
import { WeatherHomeComponent } from './weather/weather-home/weather-home.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';


export const routes: Routes = [
  { path: 'weather', component: WeatherHomeComponent },
  { path: '', redirectTo: '/weather', pathMatch: 'full' }
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
