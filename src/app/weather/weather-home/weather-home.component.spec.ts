
import { createComponentFactory, Spectator } from '@ngneat/spectator/jest';
import { WeatherHomeComponent } from './weather-home.component';

describe('WeatherHomeComponent', () => {
  let spectator: Spectator<WeatherHomeComponent>;

  const createComponent = createComponentFactory({
    component: WeatherHomeComponent,
    providers: [
    ],
    declarations: [WeatherHomeComponent]
  });

  beforeEach(() => {
    spectator = createComponent();
  });

  it('Should create', () => {
    expect(spectator.component).toBeTruthy();
  });
  /*     it('Should render a title text ', () => {
      spectator.fixture.detectChanges();
      const title = spectator.fixture.debugElement.query(
        By.css('div.modal-complete-info__title')
      );
      expect(title.nativeElement.innerHTML).toContain(
        'ONBOARDING_MODAL.complete_company_information_tile'
      );
    });

    it('Should render a paragraph text', () => {
      spectator.fixture.detectChanges();
      const paragraph = spectator.fixture.debugElement.query(
        By.css('div.modal-complete-info__description')
      );
      expect(paragraph.nativeElement.innerHTML).toContain(
        'ONBOARDING_MODAL.complete_company_information_description'
      );
    });

    it('Should render a correct text button with !sentInvitation', () => {
      spectator.fixture.detectChanges();
      const buttons = spectator.fixture.debugElement.queryAll(By.css('button'));
      expect(buttons[0].nativeElement.innerHTML).toContain('GLOBAL.continue');
    });

    it('Should call close method when click on primary button', () => {
      spyOn(spectator.component, 'close');
      spectator.fixture.debugElement
        .query(By.css('button'))
        .nativeElement.click();
      spectator.fixture.detectChanges();
      expect(spectator.component.close).toBeCalled();
    }); */
});
