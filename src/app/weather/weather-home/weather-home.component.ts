import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { select, Store } from '@ngrx/store';
import { City, State as StateCountry } from 'country-state-city';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

import { Coord } from '../../models/meteorology.models';
import * as MeteorologyActions from '../../store/meteorology.action';

@Component({
  selector: 'app-weather-home',
  templateUrl: './weather-home.component.html',
  styleUrls: ['./weather-home.component.scss']
})
export class WeatherHomeComponent implements OnInit {
  /**
    * FormControl for the autocomplete
    * @type {FormControl}
    * @public
    *    * FormControl for the autocomplete
    * @public
    *    * options load the data to the autocomplete
    * @type {string[]}
    * @public
    *    * Filtered list when user write down
    * @type {filteredOptions}
    * @public
    *    * Selector from the store
    * @type {loadWeather$}
    * @public
    */
  myControl = new FormControl();
  currentWeather: any
  options: string[] = this.countryAndCity();
  filteredOptions!: Observable<string[]>;
  loadWeather$ = this.store.pipe(select(MeteorologyActions.retrieveMeteorologyDetail))

  /**
 * @constructor
 * @param {Store} store - the app storage.
 */
  constructor(private store: Store<any>) {
    this.loadWeather$.subscribe(data => {
      this.currentWeather = data
    })
  }

  ngOnInit(): void {
    this.filteredOptions = this.myControl.valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value))
    );
  }

  /**
  * filter the cities for each keydown
  * @param {value} type : the string that user write
  * @return { string[]} count
  * @private
  */
  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.options.filter(option => option.toLowerCase().includes(filterValue));
  }
  /**
  * load the cities and countries to show in the autocomplete
  * @return { string[]} cities and countries
  * @private
  */

  private countryAndCity(): string[] {
    let cities = (StateCountry.getAllStates()).slice(0, 50)
    return cities.map((item) => {
      return item.name + ',' + item.isoCode + ',' + item.countryCode
    })
  }
  /**
  * get the data from the input when is selected
  * @param {MatAutocompleteSelectedEvent} event : MatAutocomplete event
  * @public
  */

  selectOption(event: MatAutocompleteSelectedEvent) {
    let currentInfo = event.option.value.split(',')
    let currentCity = StateCountry.getStateByCodeAndCountry(currentInfo[1], currentInfo[2])
    let latitude = !!currentCity?.latitude ? +currentCity.latitude : 0
    let longitude = !!currentCity?.longitude ? +currentCity.longitude : 0
    this.getWeather(latitude, longitude)
  }

  /**
* execute the action with te data
* @param {latitude} event :latitude from the country
* @param {latitude} event :longitude from the country
* @public
*/

  getWeather(latitude: number, longitude: number) {
    this.store.dispatch(MeteorologyActions.retrieveMeteorologyDetail({ cood: { lat: latitude, lon: longitude } as Coord }))
  }
  /**
* it takes a random city a request the information
* @public
*/

  randomCity() {
    const cityPosition = Math.floor(Math.random() * 5000) + 1
    let currentCity = City.getAllCities()[cityPosition]
    let latitude = !!currentCity?.latitude ? +currentCity.latitude : 0
    let longitude = !!currentCity?.longitude ? +currentCity.longitude : 0
    this.getWeather(latitude, longitude)

  }
}
