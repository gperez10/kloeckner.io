import { createAction, props } from '@ngrx/store';
import { Coord, Weather } from '../models/meteorology.models'
/**
   * Action trigger fo request the weather data
   * @param {Coord} cood : Coordinate for the country
   * @export
   */
export const retrieveMeteorologyDetail = createAction(
  "[Meteorology] Retrieve Meteorology detail",
  props<{ cood: Coord }>()
);
/**
   * Action Success
   * @param {Weather} meteorology : Action Success to update the state
   * @export
   */
export const retrieveMeteorologyDetailSuccess = createAction(
  "[Meteorology] Retrieve Meteorology detail Success",
  props<{ meteorology: Weather }>()
);
/**
   * Action error
   * @param {Any} error : Error to update the state
   * @export
   */
export const retrieveMeteorologyDetailFail = createAction(
  "[Meteorology] Retrieve Meteorology Fail",
  props<{ error: any }>()
);
