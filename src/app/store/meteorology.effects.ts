import { error } from '@angular/compiler/src/util';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs/internal/observable/of';
import { catchError, map, mergeMap } from 'rxjs/operators';
import { Coord, Weather } from '../models/meteorology.models';
import { WeatherService } from '../services/weather/weather.service';
import * as meteorologyActions from './meteorology.action';

@Injectable()
export class MeteorologyEffect {
  constructor(
    private actions$: Actions,
    private weatherService: WeatherService
  ) { }

  /**
  * Effect to get the weather data
  * @param {cood} data.cood : Coordinate
  * @public
  */
  loadWeather$ = createEffect(() =>
    this.actions$.pipe(
      ofType(meteorologyActions.retrieveMeteorologyDetail),
      mergeMap((data) =>
        this.weatherService
          .getWeatherByCoordiantes(data.cood)
          .pipe(
            map(
              (meteorology: Weather) =>
                meteorologyActions.retrieveMeteorologyDetailSuccess({ meteorology })
            ),
            catchError(err => of(meteorologyActions.retrieveMeteorologyDetailFail(err)))
          )
      )
    )
  );
}
