import { Weather } from './../models/meteorology.models';
import { createFeatureSelector, createSelector } from '@ngrx/store';
import { MeteorologyState } from './meteorology.reducer';



const getWeatherByCoordiantesFeatureState = createFeatureSelector<MeteorologyState>(
  'weatherData'
);
/**
  * selector to get the data from store
  * @return {Weather} weatherData
  * @export
  */
export const getWeatherByCoordiantes = createSelector(
  getWeatherByCoordiantesFeatureState,
  (state: MeteorologyState) => state.weatherData
);



