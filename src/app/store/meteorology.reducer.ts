import { createReducer, on } from '@ngrx/store';
import { Meteorology } from '../models/meteorology.models';
import * as MeteorologyActions from './meteorology.action'

export interface MeteorologyState {
  weatherData: Meteorology | null;
}

const initialState: MeteorologyState = {
  weatherData: null
};
/**
 * Reduce for meteorology state
 * @public
 */

const _meteorologyReducer = createReducer(
  initialState,
  on(MeteorologyActions.retrieveMeteorologyDetail, state => ({
    ...state,
    meteorology: null
  })),
  on(
    MeteorologyActions.retrieveMeteorologyDetailSuccess,
    (state, { meteorology }) => ({
      ...state,
      meteorology
    })
  ),
  on(MeteorologyActions.retrieveMeteorologyDetailFail, (state, { error }) => ({
    ...state,
    error
  }))
);



export function meteorologyReducer(state: any, action: any) {
  return _meteorologyReducer(state, action);
}
