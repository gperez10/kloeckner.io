import { Meteorology } from '../models/meteorology.models'

export interface AppState {
  meteorology: Meteorology;
}
