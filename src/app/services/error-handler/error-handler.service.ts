import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ErrorModalHandlerComponent } from 'src/app/share/modal/error-modal-handler/error-modal-handler.component';

@Injectable()
export class ErrorDialogService {
  public isDialogOpen: Boolean = false;
  constructor(public dialog: MatDialog) { }
  /**
 * Error dialog to show any error message
 * @param {string} data : Error message
 * @public
 */
  openDialog(data: any): any {
    if (this.isDialogOpen) {
      return false;
    }
    this.isDialogOpen = true;
    this.dialog.open(ErrorModalHandlerComponent, {
      width: '500px',
      data
    });
  }
}
