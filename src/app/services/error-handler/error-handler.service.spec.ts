
import { createComponentFactory, Spectator } from '@ngneat/spectator/jest';
import { ErrorDialogService } from './error-handler.service';



describe('ErrorDialogService', () => {
  let spectator: Spectator<ErrorDialogService>;

  const createComponent = createComponentFactory({
    component: ErrorDialogService,
    providers: [
    ],
    declarations: [ErrorDialogService]
  });

  beforeEach(() => {
    spectator = createComponent();
  });

  it('Should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
