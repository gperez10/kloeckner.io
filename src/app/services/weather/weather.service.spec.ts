
import { createComponentFactory, Spectator } from '@ngneat/spectator/jest';
import { WeatherService } from './weather.service';



describe('WeatherService', () => {
  let spectator: Spectator<WeatherService>;

  const createComponent = createComponentFactory({
    component: WeatherService,
    providers: [
    ],
    declarations: [WeatherService]
  });

  beforeEach(() => {
    spectator = createComponent();
  });

  it('Should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
