//api.service.ts
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { Coord, Weather } from 'src/app/models/meteorology.models';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  URL = `${environment.proxy}${environment.url}`
  constructor(private http: HttpClient) { }
  /**
   * http request to get the data from the server
   * @param {coord} coord : object with lat and lon
   * @public
   */
  getWeatherByCoordiantes(coord: Coord): Observable<Weather> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
      params: new HttpParams({
        fromObject: {
          lat: coord.lat,
          lon: coord.lon
        }
      }),
    };
    return this.http
      .get<any>(this.URL, httpOptions)
      .pipe(
        retry(2),
      );
  }

}
