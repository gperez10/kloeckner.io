import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-error-modal-handler',
  templateUrl: './error-modal-handler.component.html',
  styleUrls: ['./error-modal-handler.component.scss']
})
export class ErrorModalHandlerComponent implements OnInit {
  error: any
  constructor(@Inject(MAT_DIALOG_DATA) public data: string) { }

  ngOnInit(): void {
  }

}
