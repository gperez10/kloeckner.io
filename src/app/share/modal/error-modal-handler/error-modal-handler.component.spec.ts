
import { createComponentFactory, Spectator } from '@ngneat/spectator/jest';
import { ErrorModalHandlerComponent } from './error-modal-handler.component';



describe('ErrorModalHandlerComponent', () => {
  let spectator: Spectator<ErrorModalHandlerComponent>;

  const createComponent = createComponentFactory({
    component: ErrorModalHandlerComponent,
    providers: [
    ],
    declarations: [ErrorModalHandlerComponent]
  });

  beforeEach(() => {
    spectator = createComponent();
  });

  it('Should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
